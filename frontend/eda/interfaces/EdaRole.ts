import { RoleDetail, RoleRef } from './generated/eda-api';
export type EdaRole = RoleDetail;
export type EdaRoleRef = RoleRef;
